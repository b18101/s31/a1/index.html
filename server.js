/*B.
  Create a new http server in index.js using the http module of node.
	This http server should run on port 5000.

	Create routes for the following endpoints:

	1. /
		-write the appropriate headers for our response with writeHead()
			-status 200, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Welcome to B182 Booking System."

	2. /courses
		-write the appropriate headers for our response with writeHead()
			-status 200, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Welcome to the Courses Page. View our Courses."

	3. /profile
		-write the appropriate headers for our response with writeHead()
			-status 200, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Welcome to your Profile. View your details."

	4. add an else which will be a route for the undefined and undesignated endpoints.
		-write the appropriate headers for our response with writeHead()
			-status 404, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Resource not found."
	
	To run your server, go to your activity folder and open gitbash/terminal then
	run the server with nodemon/ node index.js


*/


const http = require("http");

const port = 5000;

const server = http.createServer((req, res) => {

	if(req.url === '/'){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to B182 Booking System.')
	} else if(req.url === '/courses') {
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to the Courses Page. View our Courses.')
	} else if(req.url === '/profile') {
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to your Profile. View your details.')
	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Resource not found.')
	}

})

server.listen(port);
console.log(`Server is now connected at localhost: ${port}.`);